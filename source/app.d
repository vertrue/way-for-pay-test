import vibe.utils.dictionarylist;
import std.stdio;
import std.uuid;
import std.string;
import std.conv;
import vibe.http.server;
import vibe.http.router;
import core.time;
import vibe.core.core;
import vibe.core.log;
import vibe.data.json;
import vibe.http.client;
import std.datetime.systime;
import std.file;
import std.net.curl;
import std.json;
import std.ascii : LetterCase;
import std.digest;
import std.digest.md;
import std.digest.sha;
import std.string : representation;
import std.digest.hmac;
import vibe.data.serialization;
import mongodb;

Database db;

void main()
{
	db = new Database("invoices", "", "");

	auto router = new URLRouter;
	router
		.post("/newInvoice",&newInvoice)
		.post("/invoiceStatus",&invoiceStatus);
	auto l = listenHTTP("0.0.0.0:5438", router);
	runApplication();
	scope (exit) {
	    exitEventLoop(true);
	    sleep(1.msecs);
    }
    l.stopListening();
}

void newInvoice(HTTPServerRequest req_end, HTTPServerResponse res_end)
{
	double amount = req_end.json["amount"].to!double;
	Json name = req_end.json["name"];
	Json price = req_end.json["price"];
	Json count = req_end.json["count"];
	string user_id = req_end.json["user_id"].to!string;
	string bot_id_ = req_end.json["bot_id"].to!string;
	string chatfuel_token_  = req_end.json["chatfuel_token"].to!string;
	string key = req_end.json["key"].to!string;
	string service_url = req_end.json["service_url"].to!string;
	string account = req_end.json["wfp_login"].to!string;
	string domain = req_end.json["wfp_domain"].to!string;
	string success = req_end.json["suc_block"].to!string;
	string error = req_end.json["err_block"].to!string;
	string pending = req_end.json["pen_block"].to!string;

	bool flag = false;

	if(flag)
		requestHTTP("https://api.wayforpay.com/api",					
			(scope req) {
				req.method = HTTPMethod.POST;
				string order = randomUUID().toString().replace("-" , "");

				Json a = Json.emptyObject;
				a["wfp_token"] = key;
				a["bot_id"] = bot_id_;
				a["chatfuel_token"] = chatfuel_token_;
				a["user_id"] = user_id;
				a["order"] = order;
				a["success"] = success;
				a["error"] = error;
				a["pending"] = pending;
				db["workspace_invoices"].insert(a);

				long time = Clock.currTime().toUnixTime();
				auto secret = key.representation;
				string s = account ~ ";" ~ domain ~ ";" ~ order ~ ";" ~ time.to!string ~ ";" ~ amount.to!string ~ ";UAH";
				for(int i = 0; i < name.length; i++)
					s ~= ";" ~ name[i].to!string;
				for(int i = 0; i < count.length; i++)
					s ~= ";" ~ count[i].to!string;
				for(int i = 0; i < price.length; i++)
					s ~= ";" ~ price[i].to!string;

				string hash = s.representation.hmac!MD5(secret).toHexString!(LetterCase.lower);
				Json post = Json.emptyObject;
				post["transactionType"] = "CHARGE";
				post["merchantAccount"] = account;
				post["merchantDomainName"] = domain;
				post["merchantTransactionType"] = "SALE";
				post["merchantTransactionSecureType"] = "AUTH";
				post["merchantSignature"] = hash;
				post["apiVersion"] = 1;
				post["orderReference"] = order;
				post["orderDate"] = time;
				post["serviceUrl"] = service_url;
				post["amount"] = amount;
				post["productName"] = name;
				post["productCount"] = count;
				post["productPrice"] = price;
				post["currency"] = "UAH";
				post["recToken"] = token;
				post["clientFirstName"] = "test";
				post["clientLastName"] = "test";
				post["clientEmail"] = "test";
				post["clientPhone"] = "test";
				post["clientCountry"] = "test";
				req.writeJsonBody(post);
			},
			(scope res){
				Json ans = parseJsonString("{\"redirect_to_blocks\": [\"In progress\"]}");
				res_end.writeJsonBody(ans);
			}
		);
	else 
		requestHTTP("https://api.wayforpay.com/api",					
			(scope req) {
				req.method = HTTPMethod.POST;
				string order = randomUUID().toString().replace("-" , "");

				Json a = Json.emptyObject;
				a["wfp_token"] = key;
				a["bot_id"] = bot_id_;
				a["chatfuel_token"] = chatfuel_token_;
				a["user_id"] = user_id;
				a["order"] = order;
				a["success"] = success;
				a["error"] = error;
				a["pending"] = pending;
				db["workspace_invoices"].insert(a);

				long time = Clock.currTime().toUnixTime();
				auto secret = key.representation;
				string s = account ~ ";" ~ domain ~ ";" ~ order ~ ";" ~ time.to!string ~ ";" ~ amount.to!string ~ ";UAH";
				for(int i = 0; i < name.length; i++)
					s ~= ";" ~ name[i].to!string;
				for(int i = 0; i < count.length; i++)
					s ~= ";" ~ count[i].to!string;
				for(int i = 0; i < price.length; i++)
					s ~= ";" ~ price[i].to!string;

				string hash = s.representation.hmac!MD5(secret).toHexString!(LetterCase.lower);
				Json post = Json.emptyObject;
				post["transactionType"] = "CREATE_INVOICE";
				post["merchantAccount"] = account;
				post["merchantDomainName"] = domain;
				post["merchantSignature"] = hash;
				post["apiVersion"] = 1;
				post["orderReference"] = order;
				post["orderDate"] = time;
				post["serviceUrl"] = service_url;
				post["amount"] = amount;
				post["productName"] = name;
				post["productCount"] = count;
				post["productPrice"] = price;
				post["currency"] = "UAH";
				req.writeJsonBody(post);
			},
			(scope res){
				Json resp = res.readJson;
				string final_url = resp["invoiceUrl"].get!string;
				Json ans = parseJsonString("{\"set_attributes\":{\"invoice_url\":\""~final_url~"\"}}");
				res_end.writeJsonBody(ans);
			}
		);
}

void invoiceStatus(HTTPServerRequest req_end, HTTPServerResponse res_end)
{
    string q = req_end.form.toString(), w = "";
    for(int i = 0; i < q.length; i++)
        if(i > 1 && i < q.length - 6 && q[i]!='\\')
            w ~= q[i];
   	Json requ = parseJsonString(w);
	string order = requ["orderReference"].to!string, user_id, bot_id, chatfuel_token, key;
	long time = Clock.currTime().toUnixTime();
	if(db["workspace_invoices"].find("order", order).length)
	{
		Json invoice = db["workspace_invoices"].find("order", order)[0];
		user_id = invoice["user_id"].to!string;
		bot_id = invoice["bot_id"].to!string;
		chatfuel_token = invoice["chatfuel_token"].to!string;
		key = invoice["wfp_token"].to!string;
		string success, error, pending;
		success = invoice["success"].to!string;
		error = invoice["error"].to!string;
		pending = invoice["pending"].to!string;

		auto secret = key.representation;

		if(requ["transactionStatus"].to!string == "Approved")
			requestHTTP("https://api.chatfuel.com/bots/" ~ bot_id ~ "/users/" ~ user_id ~ "/send?chatfuel_token=" ~ chatfuel_token ~ "&chatfuel_block_id=" ~ success,					
				(scope req) {
					req.method = HTTPMethod.POST;
					if(!db["workspace_tokens"].find("token", requ["recToken"].to!string).length)
					{
						Json token = Json.emptyObject;
						token["user_id"] = user_id;
						token["bot_id"] = bot_id;
						token["token"] = requ["recToken"].to!string;
						db["workspace_tokens"].insert(token);
					}	
				},
				(scope res){
				}
			);
		else if(requ["transactionStatus"].to!string == "Pending")
			requestHTTP("https://api.chatfuel.com/bots/" ~ bot_id ~ "/users/" ~ user_id ~ "/send?chatfuel_token=" ~ chatfuel_token ~ "&chatfuel_block_id=" ~ pending,					
				(scope req) {
					req.method = HTTPMethod.POST;
				},
				(scope res){
				}
			);
		else if(requ["transactionStatus"].to!string == "Declined")
			requestHTTP("https://api.chatfuel.com/bots/" ~ bot_id ~ "/users/" ~ user_id ~ "/send?chatfuel_token=" ~ chatfuel_token ~ "&chatfuel_block_id=" ~ error,					
				(scope req) {
					req.method = HTTPMethod.POST;
				},
				(scope res){
				}
			);

		Json post = Json.emptyObject;
		post["orderReference"] = order;
		post["status"] = "accept";
		post["time"] = time;
		post["signature"] = (order ~ ";accept;" ~ time.to!string).representation.hmac!MD5(secret).toHexString!(LetterCase.lower);
	    res_end.writeJsonBody(post);
	}
}

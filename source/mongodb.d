module mongodb;
/*
	Copyright: © 2014 Open Minded Webmasters
	License: Subject to the terms of the MIT license, as written in the included LICENSE.txt file.
	Authors: Lord_Evil
	Email: Lord_Evil@om-webmasters.org
*/
import vibe.db.mongo.mongo;
import vibe.data.bson;

import std.stdio;
import std.string;

class Database
{
private:
	MongoClient dbClient;
	string name;
public:
	this(string _db, string user=null, string password=null){
		this.name = _db;
		/*if (user && password){
			dbClient = connectMongoDB("mongodb://"~user~":"~password~"@127.0.0.1/"~_db);
		}
		else{*/
			dbClient = connectMongoDB("127.0.0.1");
		//}
	}
	~this(){
		dbClient.destroy();
	}
	Collection opIndex(string collection){
		return new Collection(this.name,collection,this.dbClient);
	}
}
class Collection
{
private:
	MongoCollection dbCollection;
	string name;
public:
	this(string database, string name, MongoClient client){
		this.name=name;
		dbCollection = client.getCollection(database~"."~name);
	}
	~this(){
		dbCollection.destroy();
	}
	//Find warks through "dot" synt., e.g.: key="a.b" (only for root keys)
	Json[] find(string key, string value,Json selector=Json.emptyObject){
		Json resp = Json.emptyObject;
		Json[] docs;

		bool isValJson;
		Bson valJ;
		try{ //check whether value is an object nor string
			valJ=serializeToBson(parseJsonString(value));
			isValJson=true;
		}catch(Throwable){
			isValJson=false;
		}

		if(isValJson){
			if(key.length>0){ //if query has only one selector, e.g.: db.client.find({sex:"m"})
				auto cursor = dbCollection.find([key:valJ],selector);
				while(!cursor.empty){
					docs~=cursor.front().toJson();
					cursor.popFront();
				}
			}else{ //for query with multiple selectors, e.g.: db.client.find({sex:"m", language:"ru"})
				auto cursor = dbCollection.find(valJ,selector);
				while(!cursor.empty){
					docs~=cursor.front().toJson();
					cursor.popFront();
				}
			}
		}else{
			if(key.length>0 && value.length>0){
				auto cursor = dbCollection.find([key:value],selector);
				while(!cursor.empty){
					docs~=cursor.front().toJson();
					cursor.popFront();
				}
			}else{
				auto cursor = dbCollection.find();
				while(!cursor.empty){
					docs~=cursor.front().toJson();
					cursor.popFront();
				}
			}
		}
		return docs;
	}
	Json findOne(string key, string value){
		//TODO: value as object
		Json resp = Json.emptyObject;
		Bson doc;
		if(key=="_id"){
			BsonObjectID id = BsonObjectID.fromString(value);
			doc = dbCollection.findOne([key:id]);
		}else{
			doc = dbCollection.findOne([key:value]);
		}
		if(doc!=Bson(null)){
			resp = doc.toJson();
		}else{
			resp=Json.emptyObject;
		}
		return resp;
	}

	void insert(string key, string value){
		dbCollection.insert([key:value]);
	}
	void insert(string key, Json value){
		dbCollection.insert([key:value]);
	}
	void insert(Json data){
		dbCollection.insert(serializeToBson(data));
	}
	
	void update(string key, string value, string upkey, string upval){
		Bson doc;
		Bson upvalJ;
		bool isUpvalJson;
		bool single; //case updating whole object
		if(upkey.length>0) single=false;
		else single=true;

		try{
			upvalJ=serializeToBson(parseJsonString(upval));
			isUpvalJson=true;
		}catch(Throwable){
			isUpvalJson=false;
		}
		if(key=="_id"){
			BsonObjectID id = BsonObjectID.fromString(value);
			doc = dbCollection.findOne([key:id]);
			if(doc is Bson(null)) {
				return;
			}
		}else{
			doc = dbCollection.findOne([key:value]);
		}
		if(doc is Bson(null)) {
		//some black magic here - if object does not exist then we create a new one and create a full path for the updating document:value
			doc=Bson.emptyObject;
			string[] path=key.split(".");
			Bson newobj;
			Bson latest = value;
			for(int i=cast(int)path.length-1;i>-1;i--){
				newobj=Bson.emptyObject;
				newobj[path[i]]=latest;
				latest=newobj;
			}
			doc=latest;
			dbCollection.update([key:value],doc,UpdateFlags.Upsert);
		}
		//insert whole object
		if(single){
			if(isUpvalJson)
				if(key=="_id"){
					BsonObjectID id = BsonObjectID.fromString(value);
					dbCollection.findAndModify([key:id],["$set":upvalJ]);
				}else{
					dbCollection.findAndModify([key:value],["$set":upvalJ]);
				}
			else
				writeln("ERROR - EMPTY KEY");
			return;
		}
		//more black magic - set any value for any key even if it does not exist it will be created;
		string[] path=upkey.split(".");
		if(path.length>1){
			Bson ret=doc;
			string[] newpath;
			for(int i=0;i<path.length;i++){
				if(ret.opIndex(path[i])!=Bson(null)){
					ret=ret.opIndex(path[i]);
						newpath~=path[i];
				}else{
					try{
						newpath~=path[i];
					//	writeln("Trying to set path "~newpath.join("."));
						if(key=="_id"){
							BsonObjectID id = BsonObjectID.fromString(value);
							ret=dbCollection.findAndModify([key:id],["$set":[(newpath.join(".")):Bson.emptyObject]]);
						}else{
							ret=dbCollection.findAndModify([key:value],["$set":[(newpath.join(".")):Bson.emptyObject]]);
						}
					}catch(Exception e){
						writeln("Failed: "~e.msg);
					}
				}
			}
			try{
				if(isUpvalJson)
					if(key=="_id"){
						BsonObjectID id = BsonObjectID.fromString(value);
						dbCollection.findAndModify([key:id],["$set":[newpath.join("."):upvalJ]]);
					}else{
						dbCollection.findAndModify([key:value],["$set":[newpath.join("."):upvalJ]]);
					}
				else
					if(key=="_id"){
						BsonObjectID id = BsonObjectID.fromString(value);
						dbCollection.findAndModify([key:id],["$set":[newpath.join("."):upval]]);
					}else{
						dbCollection.findAndModify([key:value],["$set":[newpath.join("."):upval]]);
					}
			}catch(Exception e){
				writeln("Failed: "~e.msg);
			}
		}else{
			if(isUpvalJson){
				doc[path[0]]=upvalJ;
				if(key=="_id"){
					BsonObjectID id = BsonObjectID.fromString(value);
					dbCollection.update([key:id],["$set":[path[0]:upvalJ]],UpdateFlags.Upsert);
				}else{
					dbCollection.update([key:value],["$set":[path[0]:upvalJ]],UpdateFlags.Upsert);
				}
			}
			else{
				doc[path[0]]=upval;
				if(key=="_id"){
					BsonObjectID id = BsonObjectID.fromString(value);
					dbCollection.update([key:id],["$set":[path[0]:upval]],UpdateFlags.Upsert);
				}else{
					dbCollection.update([key:value],["$set":[path[0]:upval]],UpdateFlags.Upsert);
				}
			}
		}	
	}
/*	void update(string key, string value, string upkey, Json upval){

	}
*/
	void updateRange(Json query, string upkey, string upval){
		dbCollection.update(serializeToBson(query),["$set":[upkey:upval]],UpdateFlags.MultiUpdate);
	}
	//void update(string m_coll){}
	void remove(string key, string value){
		if(key==""){
			dbCollection.remove();
		}
		if(key=="_id"){
			BsonObjectID id = BsonObjectID.fromString(value);
			dbCollection.remove([key:id],DeleteFlags.SingleRemove);
		}else{
			dbCollection.remove([key:value],DeleteFlags.SingleRemove);
		}
	}
}
